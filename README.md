# WWbbUnfolding

# Table of Contents
1. [Introduction](#introduction)
2. [Build](#build)  
3. [Sample File Structure](#file-structure)
4. [Setup](#setup)\
4a. [WWbbUnfolding.cxx](#main)\
4b. [Environment.cxx](#environment)\
4c. [UnfoldingPlots.cxx](#unfolding-plots)\
4d. [UnfoldingHistogramsList.json](#hist-list)
5. [Run](#run)

## Introduction <a name="introduction"></a>
This package is used to read in the histograms produced by the WWbb analysis code, and apply the TUnfold algorithm.

## Build <a name="build"></a>
```
$ mkdir WWbbUnfolding
$ cd WWbbAnalysis && mkdir build run
$ git clone https://gitlab.cern.ch/<user>/wwbbanalysis.git
$ cd build 
$ cmake ../wwbbunfolding/WWbbUnfolding
$ make
```

## Sample File Structure <a name="file-structure"></a>
This is a sample file tree for the input root file, this will be referred to in the next section.

```
input_histograms_file.root    
│
└───DIJET     
│
└───DIBOSON 
│   │
│   └───UNFOLDING
│       │   reco_hist
│       │   gen_hist
|       |   mig_matrix
│       │   ...
└───TTBAR  
│   │
│   └───UNFOLDING
│       │   reco_hist
│       │   gen_hist
|       |   mig_matrix
│       │   ...
```

## Setup <a name="setup"></a>
There are a number of different fields that needs to be changed throughout the script. 

### WWbbUnfolding.cxx <a name="main"></a>
This is the main file that calls all of the different unfolding functions.

```
$ L12 Environment::InitializeEnvironment("INSERT ROOT FILE NAME HERE");
-> Insert the file name of the input root file. The directory containing the file will be setup in the Environment.cxx file.
-> In the example above the name would be input_histograms_file.root
```

### Environment.cxx <a name="environment"></a>
This file sets up some different constant variables such as plotting colors and directory names. The follow lines needs to be changed :

```
$ L19 const std::string directory = "CHANGE TO LOCATION OF DATA FILES"; 
-> Change this line to the directory containing the input root file. The input file name will be appended to the end of this file.

$ L20 const std::string json_directory = "CHANGE TO LOCATION OF JSON FILE";
-> Change this line to the directory containing the json file containing the list of histograms. 

$ L57-End m_MCSources = {}...
-> These lines contains the MC sources, these are the names of the directories in the root file. Change these in case the MC sources are different.
-> In the example above, these would DIJET, DIBOSON, TTBAR, etc.
```

### UnfoldingPlots.cxx <a name="unfolding-plots"></a>
This files contains the main functions for running the unfolding.

```
$ L3 const std::string dataHistDir = "CHANGE TO DATA FILE LOCATION";
-> Change to the name of the directory in the input file containing the data histogram.
-> In the example above, this would be UNFOLDING.

$ L4 const std::string truthHistDir = "CHANGE TO MC TRUTH FILE LOCATION";
-> Change to the name of the directory in the input file containing the truth histogram.
-> In the example above, this would be UNFOLDING.

$ L5 const std::string migMatrixDir = "CHANGE TO MIG MATRIX LOCATION";
-> Change to the name of the directory in the input file containing the migration histogram.
-> In the example above, this would be UNFOLDING.

$ L6 const std::string BGHistDir = "CHANGE TO BG HIST LOCATION";
-> Change to the name of the directory in the input file containing the BG reco histogram.
-> In the example above, this would be UNFOLDING.
```

### UnfoldingHistogramsList.json <a name="hist-list"></a>
This files contains the list of histograms to be read in.

```
"MIG_WHAD_PT" : {
    "Region" : "UNFOLDING",
    "LegendEntry" : "",
    "HistName" : "[MIG] MIG_WHAD_PT",
    "xLabel" : "Reco. WHad #font[52]{p}_{T} [GeV]",
    "yLabel" : "Gen. WHad #font[52]{p}_{T} [GeV]" ,
    "LogY" : 0,
    "Normalize" : 0,
    "SavePlot" : 1
},

```


## Run <a name="run"></a>

```
$ cd WWbbAnalysis/build
$ ./WWbbUnfolding
```
