#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options.hpp>

#include <iostream>

#include "Environment.h"
#include "UnfoldingPlots.h"

#include <TROOT.h>
#include "TApplication.h"

#include "AtlasStyle.h"

int main(int argc, char** argv){
   bool activate_tn_samples = false;

   boost::program_options::options_description Description("ALLOWED INPUT PARAMETERS");
   Description.add_options()
      ("help", "Print help message.")
      ("input,i", boost::program_options::value<std::string>()->notifier(ENVIRONMENT::InitializeInputFile), "Input file name.") 
      ("output,o", boost::program_options::value<std::string>()->notifier(ENVIRONMENT::InitializeOutputFile), "Output file name.")
      ("json,j", boost::program_options::value<std::string>()->notifier(ENVIRONMENT::InitializeJsonFile), "Input json file name.")
      ("tn,t", boost::program_options::bool_switch(&activate_tn_samples), "Reading in TN samples.");

   boost::program_options::variables_map variables_map;
   boost::program_options::store(boost::program_options::parse_command_line(argc, argv, Description), variables_map);
   boost::program_options::notify(variables_map);
   if(variables_map.count("help")){ std::cout << Description << std::endl; return 1;}

   if(activate_tn_samples) ENVIRONMENT::ActivateTNSamples(); 

   TApplication fooApp("fooApp", &argc, argv);
   SetAtlasStyle();

   UnfoldingPlots::GeneratePlots();

   fooApp.Run();
}
