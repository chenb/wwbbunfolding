#include "CommonToolsLines.h"

void COMMONTOOLSLINES::DrawSplitCanvasSeparator(double fxMin, double fxMax){
   TLine* BlackLine = new TLine(fxMin, 0., fxMax, 0.);
   BlackLine->SetLineWidth(2);
   BlackLine->Draw();
}

void COMMONTOOLSLINES::DrawRatioPlotDashedLines(double fxMin, double fxMax, double fYOffset){
   TLine* line1 = new TLine(fxMin, 1 - (2*(fYOffset/2.5)), fxMax, 1 - (2*(fYOffset/2.5)));
   line1->SetLineStyle(7);
   line1->Draw();
   
   TLine* line2 = new TLine(fxMin, 1 - (fYOffset/2.5), fxMax, 1 - (fYOffset/2.5));
   line2->SetLineStyle(7);
   line2->Draw();
   
   TLine* line3 = new TLine(fxMin, 1., fxMax, 1.);
   line3->SetLineStyle(7);
   line3->Draw();
   
   TLine* line4 = new TLine(fxMin, 1 + (fYOffset/2.5), fxMax, 1 + (fYOffset/2.5));
   line4->SetLineStyle(7);
   line4->Draw();
   
   TLine* line5 = new TLine(fxMin, 1 + (2*(fYOffset/2.5)), fxMax, 1 + (2*(fYOffset/2.5)));
   line5->SetLineStyle(7);
   line5->Draw();
}
