#include "Environment.h"

#include <iostream>

void ENVIRONMENT::InitializeInputFile(const std::string& fInputFileName){
   ENVIRONMENT::FILES::INPUT_FILE_NAME = fInputFileName;
   ENVIRONMENT::FILES::INPUT_FILE = TFile::Open(ENVIRONMENT::FILES::INPUT_FILE_NAME.c_str());  
}

void ENVIRONMENT::InitializeOutputFile(const std::string& fOutputFileName){
   ENVIRONMENT::FILES::OUTPUT_FILE_NAME = fOutputFileName;
}
   
void ENVIRONMENT::InitializeJsonFile(const std::string& fJsonFileName){
   ENVIRONMENT::FILES::INPUT_JSON_FILE_NAME = fJsonFileName;
}

void ENVIRONMENT::ActivateTNSamples(){
   ENVIRONMENT::FILES::ACTIVATE_TN_SAMPLES = true; 
}
