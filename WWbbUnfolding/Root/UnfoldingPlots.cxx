#include "UnfoldingPlots.h"

#include <fstream>

#include "TUnfoldDensity.h"
#include "TUnfoldSys.h"
#include "TUnfold.h"

#include "Environment.h"
#include "Constants.h"
#include "CommonTools.h"
#include "CommonToolsPlotting.h"
#include "CommonToolsLegend.h"
#include "CommonToolsLines.h"

void UnfoldingPlots::GeneratePlots(){
   nlohmann::json json_file;
   std::ifstream json_path(ENVIRONMENT::FILES::INPUT_JSON_FILE_NAME);
   json_path >> json_file;

   //PlotProjections(json_file["RECO_WHAD_PT"], json_file["GEN_WHAD_PT"], json_file["MIG_WHAD_PT"]); 
   //PerformUnfolding(json_file["RECO_WHAD_PT"], json_file["GEN_WHAD_PT"], json_file["FAKE_WHAD_PT"], json_file["MIG_WHAD_PT"]);
   PerformUnfolding(json_file["RECO_WHAD_PT"], json_file["GEN_WHAD_PT"], json_file["BG_WHAD_PT"], json_file["MIG_WHAD_PT"], false);
}

void UnfoldingPlots::PlotProjections(nlohmann::json fRecoHistJson, nlohmann::json fTruthHistJson, nlohmann::json fMigMatrixJson){
   // + Get the 1D reco histogram
   CONFIG RecoConfig = COMMONTOOLS::ConvertJsonToStruct(fRecoHistJson);
   TH1D* RecoHist = COMMONTOOLS::Initialize1DHistogram("TTBAR", "UNFOLDING", RecoConfig.HISTNAME);

   // + Get the 1D truth histogram
   CONFIG TruthConfig = COMMONTOOLS::ConvertJsonToStruct(fTruthHistJson);
   TH1D* TruthHist = COMMONTOOLS::Initialize1DHistogram("TTBAR", "UNFOLDING", TruthConfig.HISTNAME);

   // + Initialize1D the 2D migration matrix
   CONFIG MigConfig = COMMONTOOLS::ConvertJsonToStruct(fMigMatrixJson);
   TH2D* MigMatrix = COMMONTOOLS::Initialize2DHistogram("TTBAR", "UNFOLDING", MigConfig.HISTNAME);

   // + Get the X-projection of the migration matrix
   TH1D* MigXProj = MigMatrix->ProjectionX();
   COMMONTOOLS::SetHistogramVisuals(MigXProj, {.LINE_COLOR = ENVIRONMENT::BASE_COLORS::BLUE, .MARKER_COLOR = ENVIRONMENT::BASE_COLORS::BLUE});

   // + Get the Y-projection of the Migration matrix
   TH1D* MigYProj = MigMatrix->ProjectionY();
   COMMONTOOLS::SetHistogramVisuals(MigYProj, {.LINE_COLOR = ENVIRONMENT::BASE_COLORS::BLUE, .MARKER_COLOR = ENVIRONMENT::BASE_COLORS::BLUE});

   std::vector<LEGENDENTRY> LegendEntries;
   LegendEntries.push_back(LEGENDENTRY{RecoHist, "Nominal", "LP"});
   LegendEntries.push_back(LEGENDENTRY{MigXProj, "Projection", "LP"});

   std::string ProjXCanvasName = RecoConfig.FILENAME + "-PROJX";
   TCanvas* ProjXCanvas = new TCanvas(ProjXCanvasName.c_str(), ProjXCanvasName.c_str(), CANVAS_SIZE, CANVAS_SIZE); 
   COMMONTOOLSPLOTTING::PlotSplitCanvasMainPad(ProjXCanvas, RecoConfig, RecoHist, MigXProj);
   COMMONTOOLSLEGEND::BuildOneColumnLegend(true, LegendEntries);
   COMMONTOOLSPLOTTING::PlotSplitCanvasRatioPad(ProjXCanvas, RecoConfig, RecoHist, MigXProj, "Nominal / Proj.");

   std::string ProjYCanvasName = RecoConfig.FILENAME + "-PROJY";
   TCanvas* ProjYCanvas = new TCanvas(ProjYCanvasName.c_str(), ProjYCanvasName.c_str(), CANVAS_SIZE, CANVAS_SIZE); 
   COMMONTOOLSPLOTTING::PlotSplitCanvasMainPad(ProjYCanvas, TruthConfig, TruthHist, MigYProj);
   COMMONTOOLSLEGEND::BuildOneColumnLegend(true, LegendEntries);
   COMMONTOOLSPLOTTING::PlotSplitCanvasRatioPad(ProjXCanvas, RecoConfig, RecoHist, MigXProj, "Nominal / Proj.");
}

void UnfoldingPlots::PerformUnfolding(nlohmann::json fRecoHistJson, nlohmann::json fTruthHistJson, nlohmann::json fBGHistJson, nlohmann::json fMigMatrixJson, bool fSubtractionMigrationBins){
   CONFIG RecoConfig = COMMONTOOLS::ConvertJsonToStruct(fRecoHistJson);
   CONFIG TruthConfig = COMMONTOOLS::ConvertJsonToStruct(fTruthHistJson);
   CONFIG MigrationConfig = COMMONTOOLS::ConvertJsonToStruct(fMigMatrixJson);

   TH1D* DataHist = GetDataHistogram(fRecoHistJson); 
   TH1D* BGHist = GetBGHistogram(fBGHistJson);
   TH1D* TruthHist = GetTruthHistogram(fTruthHistJson);
   TH2D* MigrationMatrix= GetMigrationMatrix(fMigMatrixJson);
   
   TH1D* MigrationBGHist = new TH1D();
   if(fSubtractionMigrationBins){
      TH2D* MigrationMatrixClone = (TH2D*)MigrationMatrix->Clone();    
      MigrationBGHist = MigrationMatrixClone->ProjectionX("GenOverflowBinsHist", MigrationMatrixClone->GetNbinsY(), MigrationMatrixClone->GetNbinsY() + 1);
      COMMONTOOLS::SetHistogramVisuals(MigrationBGHist, {.LINE_COLOR = ENVIRONMENT::BASE_COLORS::BLUE, .MARKER_COLOR = ENVIRONMENT::BASE_COLORS::BLUE});
      
      for(int i = 1; i <= MigrationMatrix->GetNbinsX(); i++){
         MigrationMatrix->SetBinContent(i, MigrationMatrix->GetNbinsY() + 1, 0.);
      }
   }

   TUnfoldDensity* UnfoldRegBGSignalOnly = new TUnfoldDensity(MigrationMatrix, TUnfold::kHistMapOutputVert, TUnfold::kRegModeSize, TUnfold::kEConstraintNone);
   UnfoldRegBGSignalOnly->SetInput(DataHist, 0.);
   
   if(ENVIRONMENT::FILES::ACTIVATE_TN_SAMPLES){
      UnfoldRegBGSignalOnly->SubtractBackground(BGHist, "Background");
   }else{
      for(unsigned int i = 0; i < ENVIRONMENT::MC_SOURCES_BG.size(); i++){
         std::string iSourceName = ENVIRONMENT::MC_SOURCES_BG[i];
         TH1D* iBGHist = COMMONTOOLS::Initialize1DHistogram(iSourceName, RecoConfig.REGION, RecoConfig.HISTNAME);
         UnfoldRegBGSignalOnly->SubtractBackground(iBGHist, iSourceName.c_str());
      }
      UnfoldRegBGSignalOnly->SubtractBackground(BGHist, "FAKES");
   }

   if(fSubtractionMigrationBins){
      UnfoldRegBGSignalOnly->SubtractBackground(MigrationBGHist, "MigrationBGHist");
   } 

   TH1D* InputHistAfterBGSub = (TH1D*)UnfoldRegBGSignalOnly->GetInput("InputHistAfterBgSub");
   COMMONTOOLS::SetHistogramVisuals(InputHistAfterBGSub, {.LINE_COLOR = ENVIRONMENT::BASE_COLORS::RED, .LINE_WIDTH = 2, .MARKER_COLOR = ENVIRONMENT::BASE_COLORS::RED});

   int nScan = 30;
   double tauMin = 0;
   double tauMax = 0;
   int iBest;
   TSpline *logTauX, *logTauY;
   TGraph* lCurve;

   iBest = UnfoldRegBGSignalOnly->ScanLcurve(nScan,tauMin,tauMax,&lCurve,&logTauX,&logTauY);
   std::cout << "Finished unfolding after iteration : " << iBest << std::endl;

   TH1D* OutputRegBGSignalOnly = (TH1D*)UnfoldRegBGSignalOnly->GetOutput(RecoConfig.HISTNAME.c_str(), RecoConfig.HISTNAME.c_str());
   COMMONTOOLS::SetHistogramVisuals(OutputRegBGSignalOnly, {.LINE_WIDTH = 2});

   std::string MigMatrixCanvasName = RecoConfig.FILENAME + "-CANVAS"; 
   TCanvas* MigMatrixCanvas = new TCanvas(MigMatrixCanvasName.c_str(), MigMatrixCanvasName.c_str(), CANVAS_SIZE, CANVAS_SIZE);
   COMMONTOOLSPLOTTING::PlotSingleCanvas2D(MigMatrixCanvas, MigrationConfig, MigrationMatrix);

   std::vector<LEGENDENTRY> InputLegend;
   InputLegend.push_back(LEGENDENTRY{DataHist, "Data_{Raw}", "LP"});
   InputLegend.push_back(LEGENDENTRY{InputHistAfterBGSub, "Data_{Raw} - MC_{BG}", "LP"});

   std::string InputHistsCanvasName = RecoConfig.FILENAME +"-CANVAS";
   TCanvas* InputHistsCanvas = new TCanvas(InputHistsCanvasName.c_str(), InputHistsCanvasName.c_str(), CANVAS_SIZE, CANVAS_SIZE);
   COMMONTOOLSPLOTTING::PlotSingleCanvas1DOverlay(InputHistsCanvas, RecoConfig, std::vector<TH1D*>{DataHist, InputHistAfterBGSub});
   COMMONTOOLSLEGEND::BuildOneColumnLegend(false, InputLegend);

   std::vector<LEGENDENTRY> OutputLegend;
   OutputLegend.push_back(LEGENDENTRY{OutputRegBGSignalOnly, "Data_{Unfold}", "LP"});
   OutputLegend.push_back(LEGENDENTRY{TruthHist, "MC_{Truth}", "LP"});

   std::string OutputCanvasName = TruthConfig.FILENAME + "-CANVAS";
   TCanvas* OutputCanvas = new TCanvas(OutputCanvasName.c_str(), OutputCanvasName.c_str(), CANVAS_SIZE, CANVAS_SIZE);
   COMMONTOOLSPLOTTING::PlotSplitCanvasMainPad(OutputCanvas, TruthConfig, OutputRegBGSignalOnly, TruthHist);
   COMMONTOOLSLEGEND::BuildOneColumnLegend(true, OutputLegend);
   COMMONTOOLSPLOTTING::PlotSplitCanvasRatioPad(OutputCanvas, RecoConfig, OutputRegBGSignalOnly, TruthHist, "Data / MC");

   ////TH2* covMatrix = unfoldRegBG->GetEmatrixTotal("Unfolding total covariance matrix"); 
   ////covMatrix->GetXaxis()->SetTitle(truth_xLabel.c_str());
   ////covMatrix->GetYaxis()->SetTitle(truth_xLabel.c_str());
   ////TCanvas* covariance = new TCanvas("covariance", "covariance", 800, 800);
   ////covariance->SetTopMargin(0.06);
   ////covariance->SetLeftMargin(0.15);
   ////covariance->SetRightMargin(0.15);
   ////covMatrix->Draw("COLZ");

   ////TH2* corMatrix = unfoldRegBG->GetRhoIJtotal("Unfolding total correlation matrix"); 
   ////corMatrix->GetXaxis()->SetTitle(truth_xLabel.c_str());
   ////corMatrix->GetYaxis()->SetTitle(truth_xLabel.c_str());
   ////TCanvas* correlation = new TCanvas("correlation", "correlation", 800, 800);
   ////correlation->SetTopMargin(0.06);
   ////correlation->SetLeftMargin(0.15);
   ////correlation->SetRightMargin(0.15);
   ////corMatrix->Draw("COLZ");
}

TH1D* UnfoldingPlots::GetDataHistogram(nlohmann::json fRecoHistJson){
   CONFIG RecoConfig = COMMONTOOLS::ConvertJsonToStruct(fRecoHistJson);
   std::string directoryName = "DATA";
   if(ENVIRONMENT::FILES::ACTIVATE_TN_SAMPLES) directoryName = "";
   TH1D* DataHist = COMMONTOOLS::Initialize1DHistogram(directoryName, RecoConfig.REGION, RecoConfig.HISTNAME, 
         {.LINE_WIDTH = 2});
   return DataHist;
}

TH1D* UnfoldingPlots::GetBGHistogram(nlohmann::json fBGHistJson){
   CONFIG BGConfig = COMMONTOOLS::ConvertJsonToStruct(fBGHistJson);
   std::string directoryName = "DATA";
   if(ENVIRONMENT::FILES::ACTIVATE_TN_SAMPLES) directoryName = "";
   TH1D* BGHist = COMMONTOOLS::Initialize1DHistogram(directoryName, BGConfig.REGION, BGConfig.HISTNAME);
   return BGHist;
}
   
TH1D* UnfoldingPlots::GetTruthHistogram(nlohmann::json fTruthHistJson){
   CONFIG TruthConfig = COMMONTOOLS::ConvertJsonToStruct(fTruthHistJson);
   if(ENVIRONMENT::FILES::ACTIVATE_TN_SAMPLES){
      TH1D* TruthHist = COMMONTOOLS::Initialize1DHistogram("", TruthConfig.REGION, TruthConfig.HISTNAME, 
            {.LINE_COLOR = ENVIRONMENT::BASE_COLORS::BLUE, .LINE_WIDTH = 2, .MARKER_COLOR = ENVIRONMENT::BASE_COLORS::BLUE});
      return TruthHist; 
   }
   
   TH1D* TruthHist = COMMONTOOLS::Initialize1DHistogram("RARETOP", "NOSYS/RECOFIDUCIAL", "[WHAD] WHAD_PT");
   for(unsigned int i = 0; i < ENVIRONMENT::MC_SOURCES_SIGNAL.size(); i++){
      std::string iSourceName = ENVIRONMENT::MC_SOURCES_SIGNAL[i];
      if(iSourceName == "RARETOP") continue;
      TH1D* iTruthHist = (TH1D*)COMMONTOOLS::Initialize1DHistogram(iSourceName, TruthConfig.REGION, TruthConfig.HISTNAME)->Clone();
      TruthHist->Add(iTruthHist);
   }
   COMMONTOOLS::SetHistogramVisuals(TruthHist, {.LINE_COLOR = ENVIRONMENT::BASE_COLORS::BLUE, .LINE_WIDTH = 2, .MARKER_COLOR = ENVIRONMENT::BASE_COLORS::BLUE});
   return TruthHist;
}

TH2D* UnfoldingPlots::GetMigrationMatrix(nlohmann::json fMigrationMatrixJson){
   CONFIG MatrixConfig = COMMONTOOLS::ConvertJsonToStruct(fMigrationMatrixJson);
   if(ENVIRONMENT::FILES::ACTIVATE_TN_SAMPLES){
      TH2D* MigrationMatrix = COMMONTOOLS::Initialize2DHistogram("", MatrixConfig.REGION, MatrixConfig.HISTNAME);
      return MigrationMatrix;
   }

   const std::string reco_unfolding_region = "NOSYS/UNFOLDING_RECO";
   const std::string gen_unfolding_region = "NOSYS/UNFOLDING_GEN";

   TH2D* MigrationMatrix = COMMONTOOLS::Initialize2DHistogram("RARETOP", "NOSYS/UNFOLDING_RECO", MatrixConfig.HISTNAME);
   for(unsigned int i = 0; i < ENVIRONMENT::MC_SOURCES_SIGNAL.size(); i++){
      std::string iSourceName = ENVIRONMENT::MC_SOURCES_SIGNAL[i]; 
      if(iSourceName == "RARETOP") continue;
      TH2D* iRecoMigrationMatrix = (TH2D*)COMMONTOOLS::Initialize2DHistogram(iSourceName, reco_unfolding_region, MatrixConfig.HISTNAME)->Clone();
      TH2D* iGenMigrationMatrix = (TH2D*)COMMONTOOLS::Initialize2DHistogram(iSourceName, gen_unfolding_region, MatrixConfig.HISTNAME)->Clone();
      MigrationMatrix->Add(iRecoMigrationMatrix);
      MigrationMatrix->Add(iGenMigrationMatrix);
   }

   return MigrationMatrix;
}


