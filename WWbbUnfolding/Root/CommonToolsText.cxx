#include "CommonToolsText.h"

void COMMONTOOLSTEXT::SplitAndPrintText(const std::string& fText, bool fIsSplitCanvas){
   std::stringstream InputString(fText);
   std::string Segment;
   std::vector<std::string> SegmentList;
   
   double LABEL_X = fIsSplitCanvas ? RATIO_LABEL_X : SINGLE_LABEL_X;
   double LABEL_Y = fIsSplitCanvas ? RATIO_LABEL_Y : SINGLE_LABEL_Y;
   double TEXT_BASE_Y = fIsSplitCanvas ? RATIO_TEXT_BASE_Y : SINGLE_TEXT_BASE_Y;
   double TEXT_INC_Y = fIsSplitCanvas ? RATIO_TEXT_INC_Y : SINGLE_TEXT_INC_Y;
   double TEXT_SIZE = fIsSplitCanvas ? RATIO_TEXT_SIZE : SINGLE_TEXT_SIZE;

   DrawATLASLabel(LABEL_X, LABEL_Y, fIsSplitCanvas, "Work in Progress");

   if(fText.empty()) return;

   while(std::getline(InputString, Segment, '^')){
      SegmentList.push_back(Segment);
   }

   for(unsigned int i = 0; i < SegmentList.size(); i++){
      TLatex* iStatus = new TLatex(LABEL_X, LABEL_Y - (TEXT_BASE_Y + i * TEXT_INC_Y), SegmentList[i].c_str());
      iStatus->SetTextFont(42);
      iStatus->SetTextSize(TEXT_SIZE);
      iStatus->SetNDC();
      iStatus->Draw();
   }
}

void COMMONTOOLSTEXT::DrawATLASLabel(double fX, double fY, bool fIsSplitCanvas, const std::string& fText){
   
   double TextSize = fIsSplitCanvas ? RATIO_TEXT_SIZE : SINGLE_TEXT_SIZE;
   double x_inc = 0.10;
   double y_inc = fIsSplitCanvas ? 0.05 : 0.035;

   TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize); 
   l.SetNDC();
   l.SetTextFont(72);
   l.SetTextSize(TextSize);
   l.SetTextColor(kBlack);

   l.DrawLatex(fX,fY,"ATLAS");
   if (!fText.empty()) {
      TLatex p; 
      p.SetNDC();
      p.SetTextFont(42);
      p.SetTextSize(TextSize);
      p.SetTextColor(kBlack);
      p.DrawLatex(fX + x_inc, fY, fText.c_str());
      p.DrawLatex(fX, fY - y_inc, "#sqrt{s} = 13 TeV, 44.3 fb^{-1}");
   }
}

