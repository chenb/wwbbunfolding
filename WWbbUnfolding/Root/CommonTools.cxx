#include "CommonTools.h"

#include "Environment.h"

#include <iostream>

CONFIG COMMONTOOLS::ConvertJsonToStruct(const nlohmann::json& fInputJson){
   CONFIG PlotConfig;

   PlotConfig.REGION      = std::string(fInputJson["Region"]);
   PlotConfig.LEGENDENTRY = std::string(fInputJson["LegendEntry"]);
   PlotConfig.HISTNAME    = std::string(fInputJson["HistName"]);
   PlotConfig.FILENAME    = std::string(fInputJson["FileName"]);
   PlotConfig.XLABEL      = std::string(fInputJson["xLabel"]);
   PlotConfig.YLABEL      = std::string(fInputJson["yLabel"]);
   PlotConfig.TEXT        = std::string(fInputJson["Text"]);
   PlotConfig.LOGY        = fInputJson["LogY"];
   PlotConfig.NORMALIZE   = fInputJson["Normalize"];
   PlotConfig.SAVEPLOT    = fInputJson["SavePlot"];

   return PlotConfig;
}

TH1D* COMMONTOOLS::Initialize1DHistogram(const std::string& fSource, const std::string& fRegion, const std::string& fHistName, const VISUAL& fHistVisuals){
   std::string HistPath = fSource + "/" + fRegion + "/" + fHistName;
   TH1D* InputHistogram = (TH1D*)ENVIRONMENT::FILES::INPUT_FILE->Get(HistPath.c_str());

   if(InputHistogram == nullptr){
      std::cout << "Input histogram : " << HistPath << " not found! Exiting." << std::endl;
      exit(1);
   }

   InputHistogram->SetLineColor(fHistVisuals.LINE_COLOR);
   InputHistogram->SetLineStyle(fHistVisuals.LINE_STYLE);
   InputHistogram->SetLineWidth(fHistVisuals.LINE_WIDTH);

   InputHistogram->SetFillColor(fHistVisuals.FILL_COLOR);
   InputHistogram->SetFillStyle(fHistVisuals.FILL_STYLE);
   
   InputHistogram->SetMarkerColor(fHistVisuals.MARKER_COLOR);
   InputHistogram->SetMarkerStyle(fHistVisuals.MARKER_STYLE);
   InputHistogram->SetMarkerSize(fHistVisuals.MARKER_SIZE);

   return InputHistogram;
}

TH2D* COMMONTOOLS::Initialize2DHistogram(const std::string& fSource, const std::string& fRegion, const std::string& fHistName){
   std::string HistPath = fSource + "/" + fRegion + "/" + fHistName;
   TH2D* InputHistogram = (TH2D*)ENVIRONMENT::FILES::INPUT_FILE->Get(HistPath.c_str());

   if(InputHistogram == nullptr){
      std::cout << "Input histogram : " << HistPath << " not found! Exiting." << std::endl;
      exit(1);
   }

   return InputHistogram;
}

void COMMONTOOLS::SetHistogramVisuals(TH1D* fInputHistogram, const VISUAL& fHistVisuals){
   fInputHistogram->SetLineColor(fHistVisuals.LINE_COLOR);
   fInputHistogram->SetLineStyle(fHistVisuals.LINE_STYLE);
   fInputHistogram->SetLineWidth(fHistVisuals.LINE_WIDTH);

   fInputHistogram->SetFillColor(fHistVisuals.FILL_COLOR);
   fInputHistogram->SetFillStyle(fHistVisuals.FILL_STYLE);
   
   fInputHistogram->SetMarkerColor(fHistVisuals.MARKER_COLOR);
   fInputHistogram->SetMarkerStyle(fHistVisuals.MARKER_STYLE);
   fInputHistogram->SetMarkerSize(fHistVisuals.MARKER_SIZE);
}

RANGE COMMONTOOLS::GetHistRange(TH1D* fInputHist, THStack* fStackedHist){
   RANGE HistRange; 
   HistRange.XMIN = fInputHist->GetXaxis()->GetXmin();
   HistRange.XMAX = fInputHist->GetXaxis()->GetXmax();
   HistRange.YMIN = 0.;
   HistRange.YMAX = (fInputHist->GetMaximum() > fStackedHist->GetMaximum()) ? fInputHist->GetMaximum() : fStackedHist->GetMaximum();

   return HistRange;
}

RANGE COMMONTOOLS::GetHistRange(TH1D* fInputHista, TH1D* fInputHistb){
   RANGE HistRange; 
   HistRange.XMIN = fInputHista->GetXaxis()->GetXmin();
   HistRange.XMAX = fInputHista->GetXaxis()->GetXmax();
   HistRange.YMIN = 0.;
   HistRange.YMAX = (fInputHista->GetMaximum() > fInputHistb->GetMaximum()) ? fInputHista->GetMaximum() : fInputHistb->GetMaximum();

   return HistRange;
}

RANGE COMMONTOOLS::GetHistRange(TH1D* fInputHist){
   RANGE HistRange; 
   HistRange.XMIN = fInputHist->GetXaxis()->GetXmin();
   HistRange.XMAX = fInputHist->GetXaxis()->GetXmax();
   HistRange.YMIN = 0.;
   HistRange.YMAX = fInputHist->GetMaximum();

   return HistRange;
}

RANGE COMMONTOOLS::GetHistRange(TH2D* fInputHist){
   RANGE HistRange; 
   HistRange.XMIN = fInputHist->GetXaxis()->GetXmin();
   HistRange.XMAX = fInputHist->GetXaxis()->GetXmax();
   HistRange.YMIN = fInputHist->GetYaxis()->GetXmin();
   HistRange.YMAX = fInputHist->GetYaxis()->GetXmax();

   return HistRange;
}

RANGE COMMONTOOLS::GetHistRange(const std::vector<TH1D*>& fInputHists){
   RANGE HistRange;
   HistRange.XMIN = fInputHists[0]->GetXaxis()->GetXmin();
   HistRange.XMAX = fInputHists[0]->GetXaxis()->GetXmax();
   HistRange.YMIN = 0.;
   HistRange.YMAX = fInputHists[0]->GetMaximum();

   for(unsigned int i = 0; i < fInputHists.size(); i++){
      if(fInputHists[i]->GetMaximum() > HistRange.YMAX) HistRange.YMAX = fInputHists[i]->GetMaximum();
   }

   return HistRange;
}


//double CommonTools::ConvertToUserX(double fX){
   //gPad->Update();
   //return fX*(gPad->GetX2() - gPad->GetX1()) + gPad->GetX1();
//}

//double CommonTools::ConvertToUserY(double fY){
   //gPad->Update();
   //return fY*(gPad->GetY2() - gPad->GetY1()) + gPad->GetY1();
//}

