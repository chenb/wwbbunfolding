#include "CommonToolsPlotting.h"

#include "CommonToolsLines.h"

void COMMONTOOLSPLOTTING::PlotSplitCanvasMainPad(TCanvas* fMainCanvas, const CONFIG& fPlotConfig, TH1D* fNumerHist, THStack* fDenomHist){
   fMainCanvas->cd();
   std::string MainPadName = fPlotConfig.FILENAME + "-MAINPAD";
   COMMONTOOLSPLOTTING::SetupSplitCanvasMainPad(MainPadName, fPlotConfig, fNumerHist, fDenomHist);

   fDenomHist->Draw("AHISTSAME");
   fNumerHist->Draw("AE1SAME");

   gPad->RedrawAxis();   
}

void COMMONTOOLSPLOTTING::PlotSplitCanvasMainPad(TCanvas* fMainCanvas, const CONFIG& fPlotConfig, TH1D* fNumerHist, TH1D* fDenomHist){
   fMainCanvas->cd();
   std::string MainPadName = fPlotConfig.FILENAME + "-MAINPAD";
   COMMONTOOLSPLOTTING::SetupSplitCanvasMainPad(MainPadName, fPlotConfig, fNumerHist, fDenomHist);

   fDenomHist->Draw("AHISTSAME");
   fNumerHist->Draw("AE1SAME");

   gPad->RedrawAxis();   
}

void COMMONTOOLSPLOTTING::PlotSplitCanvasRatioPad(TCanvas* fMainCanvas, const CONFIG& fPlotConfig, TH1D* fNumerHist, THStack* fDenomHist, const std::string& fYLabel, double fYOffset){
   fMainCanvas->cd(); 
   std::string RatioPadName = fPlotConfig.FILENAME + "-RATIO";
   COMMONTOOLSPLOTTING::SetupSplitCanvasRatioPad(RatioPadName, fPlotConfig, fNumerHist, fDenomHist, fYLabel, fYOffset);

   TH1* TopHistInStack = (TH1*)fDenomHist->GetStack()->Last();
   TGraphAsymmErrors* RatioGraph = new TGraphAsymmErrors();
   RatioGraph->Divide(fNumerHist, TopHistInStack, "pois");
   RatioGraph->SetMarkerColor(kBlack);
   RatioGraph->Draw("P SAME");   
}

void COMMONTOOLSPLOTTING::PlotSplitCanvasRatioPad(TCanvas* fMainCanvas, const CONFIG& fPlotConfig, TH1D* fNumerHist, TH1D* fDenomHist, const std::string& fYLabel, double fYOffset){
   fMainCanvas->cd(); 
   std::string RatioPadName = fPlotConfig.FILENAME + "-RATIO";
   COMMONTOOLSPLOTTING::SetupSplitCanvasRatioPad(RatioPadName, fPlotConfig, fNumerHist, fDenomHist, fYLabel, fYOffset);

   TGraphAsymmErrors* RatioGraph = new TGraphAsymmErrors();
   RatioGraph->Divide(fNumerHist, fDenomHist, "pois");
   RatioGraph->SetMarkerColor(kBlack);
   RatioGraph->Draw("P SAME");
}

void COMMONTOOLSPLOTTING::PlotSingleCanvas1D(TCanvas* fMainCanvas, const CONFIG& fPlotConfig, TH1D* fInputHist){
   COMMONTOOLSPLOTTING::SetupSingleCanvas1D(fMainCanvas, fPlotConfig, fInputHist);

   fInputHist->Draw("HIST E1");
}

void COMMONTOOLSPLOTTING::PlotSingleCanvas2D(TCanvas* fMainCanvas, const CONFIG& fPlotConfig, TH2D* fInputHist, double fzMin, double fzMax){
   COMMONTOOLSPLOTTING::SetupSingleCanvas2D(fMainCanvas, fPlotConfig, fInputHist, fzMin, fzMax);

   fInputHist->Draw("COLZ");
}

void COMMONTOOLSPLOTTING::PlotSingleCanvas1DOverlay(TCanvas* fMainCanvas, const CONFIG& fPlotConfig, const std::vector<TH1D*>& fInputHists){
   COMMONTOOLSPLOTTING::SetupSingleCanvas1DOverlay(fMainCanvas, fPlotConfig, fInputHists);
   
   for(unsigned int i = 0; i < fInputHists.size(); i++){
      fInputHists[i]->Draw("HIST E1 SAME");
   }
}

void COMMONTOOLSPLOTTING::SetupSplitCanvasMainPad(const std::string& fMainPadName, const CONFIG& fPlotConfig, TH1D* fNumerHist, THStack* fDenomHist){
   TPad* MainPad = new TPad(fMainPadName.c_str(), fMainPadName.c_str(), 0., RATIO_PAD_SIZE, 1., 1.);
   COMMONTOOLSPLOTTING::SetupSplitCanvasMainPadMargins(MainPad);

   RANGE HistRange = COMMONTOOLS::GetHistRange(fNumerHist, fDenomHist);

   TH1F* MainPadFrame = MainPad->DrawFrame(HistRange.XMIN, HistRange.YMIN, HistRange.XMAX, HistRange.YMAX * Y_AXIS_MULTIPLIER);
   COMMONTOOLSPLOTTING::SetupSplitCanvasMainPadFrame(MainPadFrame, fPlotConfig);
}

void COMMONTOOLSPLOTTING::SetupSplitCanvasMainPad(const std::string& fMainPadName, const CONFIG& fPlotConfig, TH1D* fNumerHist, TH1D* fDenomHist){
   TPad* MainPad = new TPad(fMainPadName.c_str(), fMainPadName.c_str(), 0., RATIO_PAD_SIZE, 1., 1.);
   COMMONTOOLSPLOTTING::SetupSplitCanvasMainPadMargins(MainPad);

   RANGE HistRange = COMMONTOOLS::GetHistRange(fNumerHist, fDenomHist);

   TH1F* MainPadFrame = MainPad->DrawFrame(HistRange.XMIN, HistRange.YMIN, HistRange.XMAX, HistRange.YMAX * Y_AXIS_MULTIPLIER);
   COMMONTOOLSPLOTTING::SetupSplitCanvasMainPadFrame(MainPadFrame, fPlotConfig);
}

void COMMONTOOLSPLOTTING::SetupSplitCanvasRatioPad(const std::string& fRatioPadName, const CONFIG& fPlotConfig, TH1D* fNumerHist, THStack* fDenomHist, const std::string& fYLabel, double fYOffset){
   TPad* RatioPad = new TPad(fRatioPadName.c_str(), fRatioPadName.c_str(), 0., 0., 1., RATIO_PAD_SIZE);   
   COMMONTOOLSPLOTTING::SetupSplitCanvasRatioPadMargins(RatioPad);

   RANGE HistRange = COMMONTOOLS::GetHistRange(fNumerHist, fDenomHist);

   TH1F* RatioPadFrame = RatioPad->DrawFrame(HistRange.XMIN, 1. - fYOffset, HistRange.XMAX, 1. + fYOffset);
   COMMONTOOLSPLOTTING::SetupSplitCanvasRatioPadFrame(RatioPadFrame, fPlotConfig, fYLabel);
   COMMONTOOLSLINES::DrawRatioPlotDashedLines(HistRange.XMIN, HistRange.XMAX, fYOffset);
}

void COMMONTOOLSPLOTTING::SetupSplitCanvasRatioPad(const std::string& fRatioPadName, const CONFIG& fPlotConfig, TH1D* fNumerHist, TH1D* fDenomHist, const std::string& fYLabel, double fYOffset){
   TPad* RatioPad = new TPad(fRatioPadName.c_str(), fRatioPadName.c_str(), 0., 0., 1., RATIO_PAD_SIZE);   
   COMMONTOOLSPLOTTING::SetupSplitCanvasRatioPadMargins(RatioPad);

   RANGE HistRange = COMMONTOOLS::GetHistRange(fNumerHist, fDenomHist);

   TH1F* RatioPadFrame = RatioPad->DrawFrame(HistRange.XMIN, 1. - fYOffset, HistRange.XMAX, 1. + fYOffset);
   COMMONTOOLSPLOTTING::SetupSplitCanvasRatioPadFrame(RatioPadFrame, fPlotConfig, fYLabel);
   COMMONTOOLSLINES::DrawRatioPlotDashedLines(HistRange.XMIN, HistRange.XMAX, fYOffset);
}

void COMMONTOOLSPLOTTING::SetupSingleCanvas1D(TCanvas* fMainCanvas, const CONFIG& fPlotConfig, TH1D* fInputHist){
   COMMONTOOLSPLOTTING::SetupSingleCanvas1DMargins(fMainCanvas);

   RANGE HistRange = COMMONTOOLS::GetHistRange(fInputHist);

   TH1F* MainCanvasFrame = fMainCanvas->DrawFrame(HistRange.XMIN, HistRange.YMIN, HistRange.XMAX, HistRange.YMAX * Y_AXIS_MULTIPLIER);
   COMMONTOOLSPLOTTING::SetupSingleCanvas1DFrame(MainCanvasFrame, fPlotConfig);
}

void COMMONTOOLSPLOTTING::SetupSingleCanvas2D(TCanvas* fMainCanvas, const CONFIG& fPlotConfig, TH2D* fInputHist, double fzMin, double fzMax){
   COMMONTOOLSPLOTTING::SetupSingleCanvas2DMargins(fMainCanvas); 

   RANGE HistRange = COMMONTOOLS::GetHistRange(fInputHist);

   TH1F* MainCanvasFrame = fMainCanvas->DrawFrame(HistRange.XMIN, HistRange.YMIN, HistRange.XMAX, HistRange.YMAX);
   COMMONTOOLSPLOTTING::SetupSingleCanvas2DFrame(MainCanvasFrame, fPlotConfig, fzMin, fzMax);
}

void COMMONTOOLSPLOTTING::SetupSingleCanvas1DOverlay(TCanvas* fMainCanvas, const CONFIG& fPlotConfig, const std::vector<TH1D*>& fInputHists){
   COMMONTOOLSPLOTTING::SetupSingleCanvas1DOverlayMargins(fMainCanvas);

   RANGE HistRange = COMMONTOOLS::GetHistRange(fInputHists);

   TH1F* MainCanvasFrame = fMainCanvas->DrawFrame(HistRange.XMIN, HistRange.YMIN, HistRange.XMAX, HistRange.YMAX * Y_AXIS_MULTIPLIER);
   COMMONTOOLSPLOTTING::SetupSingleCanvas1DOverlayFrame(MainCanvasFrame, fPlotConfig);
}

void COMMONTOOLSPLOTTING::SetupSplitCanvasMainPadMargins(TPad* fMainPad){
   fMainPad->SetTopMargin(MAIN_PAD_TOP_MARGIN);
   fMainPad->SetBottomMargin(MAIN_PAD_BOTTOM_MARGIN);
   fMainPad->SetRightMargin(MAIN_PAD_RIGHT_MARGIN);
   fMainPad->SetLeftMargin(MAIN_PAD_LEFT_MARGIN);
   fMainPad->Draw();
   fMainPad->cd();
}

void COMMONTOOLSPLOTTING::SetupSplitCanvasRatioPadMargins(TPad* fRatioPad){
   fRatioPad->SetTopMargin(RATIO_PAD_TOP_MARGIN); 
   fRatioPad->SetBottomMargin(RATIO_PAD_BOTTOM_MARGIN);
   fRatioPad->SetRightMargin(RATIO_PAD_RIGHT_MARGIN);
   fRatioPad->SetLeftMargin(RATIO_PAD_LEFT_MARGIN);
   fRatioPad->Draw();
   fRatioPad->cd(); 
}

void COMMONTOOLSPLOTTING::SetupSingleCanvas1DMargins(TCanvas* fMainCanvas){
   fMainCanvas->SetTopMargin(CANVAS_1D_TOP_MARGIN);
   fMainCanvas->SetBottomMargin(CANVAS_1D_BOTTOM_MARGIN);
   fMainCanvas->SetLeftMargin(CANVAS_1D_LEFT_MARGIN);
   fMainCanvas->SetRightMargin(CANVAS_1D_RIGHT_MARGIN);
   fMainCanvas->Draw();
   fMainCanvas->cd();
}

void COMMONTOOLSPLOTTING::SetupSingleCanvas2DMargins(TCanvas* fMainCanvas){
   fMainCanvas->SetTopMargin(CANVAS_2D_TOP_MARGIN);
   fMainCanvas->SetBottomMargin(CANVAS_2D_BOTTOM_MARGIN);
   fMainCanvas->SetLeftMargin(CANVAS_2D_LEFT_MARGIN);
   fMainCanvas->SetRightMargin(CANVAS_2D_RIGHT_MARGIN);
   fMainCanvas->Draw();
   fMainCanvas->cd();
}

void COMMONTOOLSPLOTTING::SetupSingleCanvas1DOverlayMargins(TCanvas* fMainCanvas){
   fMainCanvas->SetTopMargin(CANVAS_1D_TOP_MARGIN);
   fMainCanvas->SetBottomMargin(CANVAS_1D_BOTTOM_MARGIN);
   fMainCanvas->SetLeftMargin(CANVAS_1D_LEFT_MARGIN);
   fMainCanvas->SetRightMargin(CANVAS_1D_RIGHT_MARGIN);
   fMainCanvas->Draw();
   fMainCanvas->cd();
}

void COMMONTOOLSPLOTTING::SetupSplitCanvasMainPadFrame(TH1F* fMainPadFrame, const CONFIG& fPlotConfig){
   fMainPadFrame->GetXaxis()->SetTitle(fPlotConfig.XLABEL.c_str());
   fMainPadFrame->GetYaxis()->SetTitle(fPlotConfig.YLABEL.c_str());
   fMainPadFrame->GetYaxis()->ChangeLabel(1, -999, 0);
   fMainPadFrame->GetYaxis()->SetTitleOffset(RATIO_MAIN_Y_OFFSET);
   fMainPadFrame->Draw();
}

void COMMONTOOLSPLOTTING::SetupSplitCanvasRatioPadFrame(TH1F* fRatioPadFrame, const CONFIG& fPlotConfig, const std::string& fYLabel){
   fRatioPadFrame->GetXaxis()->SetTitle(fPlotConfig.XLABEL.c_str());
   fRatioPadFrame->GetXaxis()->SetLabelSize(fRatioPadFrame->GetXaxis()->GetLabelSize() * MAIN_PAD_SIZE / RATIO_PAD_SIZE);
   fRatioPadFrame->GetXaxis()->SetTitleSize(fRatioPadFrame->GetXaxis()->GetTitleSize() * MAIN_PAD_SIZE / RATIO_PAD_SIZE);
   fRatioPadFrame->GetYaxis()->SetTitle(fYLabel.c_str());
   fRatioPadFrame->GetYaxis()->CenterTitle(true);
   fRatioPadFrame->GetYaxis()->SetTitleOffset(RATIO_Y_OFFSET);
   fRatioPadFrame->GetYaxis()->SetLabelSize(fRatioPadFrame->GetYaxis()->GetLabelSize() * MAIN_PAD_SIZE / RATIO_PAD_SIZE);
   fRatioPadFrame->GetYaxis()->SetTitleSize(fRatioPadFrame->GetYaxis()->GetTitleSize() * MAIN_PAD_SIZE / RATIO_PAD_SIZE);
   fRatioPadFrame->GetYaxis()->SetNdivisions(RATIO_TICKS);
   fRatioPadFrame->Draw();
}

void COMMONTOOLSPLOTTING::SetupSingleCanvas1DFrame(TH1F* fMainCanvasFrame, const CONFIG& fPlotConfig){
   fMainCanvasFrame->GetXaxis()->SetTitle(fPlotConfig.XLABEL.c_str());
   fMainCanvasFrame->GetYaxis()->SetTitle(fPlotConfig.YLABEL.c_str());
   fMainCanvasFrame->GetYaxis()->SetTitleOffset(RATIO_MAIN_Y_OFFSET);
   fMainCanvasFrame->Draw();
}

void COMMONTOOLSPLOTTING::SetupSingleCanvas2DFrame(TH1F* fMainCanvasFrame, const CONFIG& fPlotConfig, double fzMin, double fzMax){
   fMainCanvasFrame->GetXaxis()->SetTitle(fPlotConfig.XLABEL.c_str());
   fMainCanvasFrame->GetYaxis()->SetTitle(fPlotConfig.YLABEL.c_str());
   fMainCanvasFrame->GetYaxis()->SetTitleOffset(RATIO_MAIN_Y_OFFSET);
   if(fzMin != -999) fMainCanvasFrame->SetMinimum(fzMin);
   if(fzMax != -999) fMainCanvasFrame->SetMaximum(fzMax);
   fMainCanvasFrame->Draw();
}

void COMMONTOOLSPLOTTING::SetupSingleCanvas1DOverlayFrame(TH1F* fMainCanvasFrame, const CONFIG& fPlotConfig){
   fMainCanvasFrame->GetXaxis()->SetTitle(fPlotConfig.XLABEL.c_str());
   fMainCanvasFrame->GetYaxis()->SetTitle(fPlotConfig.YLABEL.c_str());
   fMainCanvasFrame->GetYaxis()->SetTitleOffset(RATIO_MAIN_Y_OFFSET);
   fMainCanvasFrame->Draw();
}

