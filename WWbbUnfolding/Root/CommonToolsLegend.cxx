#include "CommonToolsLegend.h"
#include "Environment.h"


void COMMONTOOLSLEGEND::BuildTwoColumnLegend(bool fIsSplitCanvas, std::vector<LEGENDENTRY> fColumn1Entries, std::vector<LEGENDENTRY> fColumn2Entries){
   TLegend* Legend = new TLegend(TWO_COLUMN_LEGEND_X1, TWO_COLUMN_LEGEND_Y2 - (fColumn1Entries.size() * TWO_COLUMN_LEGEND_DELY), TWO_COLUMN_LEGEND_X2, TWO_COLUMN_LEGEND_Y2);
   Legend->SetTextFont(42);
   Legend->SetFillStyle(0);
   Legend->SetFillColor(0);
   Legend->SetBorderSize(0);
   Legend->SetNColumns(2);

   for(unsigned int i = 0; i < fColumn1Entries.size(); i++){
      Legend->AddEntry(fColumn1Entries[i].HIST, fColumn1Entries[i].LABEL.c_str(), fColumn1Entries[i].OPTION.c_str());
      Legend->AddEntry(fColumn2Entries[i].HIST, fColumn2Entries[i].LABEL.c_str(), fColumn2Entries[i].OPTION.c_str());
   }

   Legend->Draw();
}

void COMMONTOOLSLEGEND::BuildOneColumnLegend(bool fIsSplitCanvas, std::vector<LEGENDENTRY> fLegendEntries){
   double x1 = fIsSplitCanvas ? ONE_COLUMN_LEGEND_X1_SPLIT : ONE_COLUMN_LEGEND_X1;
   double x2 = fIsSplitCanvas ? ONE_COLUMN_LEGEND_X2_SPLIT : ONE_COLUMN_LEGEND_X2;
   double y2 = fIsSplitCanvas ? ONE_COLUMN_LEGEND_Y2_SPLIT : ONE_COLUMN_LEGEND_Y2;
   double dely = fIsSplitCanvas ? ONE_COLUMN_LEGEND_DELY_SPLIT : ONE_COLUMN_LEGEND_DELY;

   TLegend* Legend = new TLegend(x1, y2 - (fLegendEntries.size() * dely), x2, y2);
   Legend->SetTextFont(42);
   Legend->SetTextSize(fIsSplitCanvas ? RATIO_TEXT_SIZE : SINGLE_TEXT_SIZE);
   Legend->SetFillStyle(0);
   Legend->SetFillColor(0);
   Legend->SetBorderSize(0);
   
   for(unsigned int i = 0; i < fLegendEntries.size(); i++){
      Legend->AddEntry(fLegendEntries[i].HIST, fLegendEntries[i].LABEL.c_str(), fLegendEntries[i].OPTION.c_str());
   }

   Legend->Draw();

}

