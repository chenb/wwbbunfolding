#pragma once

#include "json.hpp"
#include "boost/variant.hpp"
#include <boost/variant/variant.hpp>

#include "Environment.h"
#include "Constants.h"
#include "CommonTools.h"

#include "TLegend.h"

struct LEGENDENTRY{
   TH1* HIST;
   std::string LABEL;
   std::string OPTION;
};

namespace COMMONTOOLSLEGEND{
   void BuildTwoColumnLegend(bool, std::vector<LEGENDENTRY>, std::vector<LEGENDENTRY>);
   void BuildOneColumnLegend(bool, std::vector<LEGENDENTRY>);
};
