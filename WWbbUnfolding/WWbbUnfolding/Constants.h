#pragma once

// + Single canvas settings
const double CANVAS_SIZE = 800.;
const double CANVAS_1D_TOP_MARGIN = 0.05;
const double CANVAS_1D_BOTTOM_MARGIN = 0.15; 
const double CANVAS_1D_RIGHT_MARGIN = 0.05;
const double CANVAS_1D_LEFT_MARGIN = 0.15;
const double CANVAS_2D_TOP_MARGIN = 0.05;
const double CANVAS_2D_BOTTOM_MARGIN = 0.15; 
const double CANVAS_2D_RIGHT_MARGIN = 0.15;
const double CANVAS_2D_LEFT_MARGIN = 0.15;

// + Split/ratio canvas settings
const double MAIN_PAD_SIZE = 0.7;
const double MAIN_PAD_TOP_MARGIN = 0.05;
const double MAIN_PAD_BOTTOM_MARGIN = 0.;
const double MAIN_PAD_RIGHT_MARGIN = 0.05;
const double MAIN_PAD_LEFT_MARGIN = 0.13;
const double RATIO_PAD_SIZE = 1. - MAIN_PAD_SIZE;
const double RATIO_PAD_TOP_MARGIN = 0.;
const double RATIO_PAD_BOTTOM_MARGIN = 0.3;
const double RATIO_PAD_RIGHT_MARGIN = 0.05;
const double RATIO_PAD_LEFT_MARGIN = 0.13;

// + Marker qualities
const double MARKER_SIZE = 0.9;

// + Legend settings
const double TWO_COLUMN_LEGEND_X1 = 0.52;
const double TWO_COLUMN_LEGEND_X2= 0.9;
const double TWO_COLUMN_LEGEND_Y2 = 0.9;
const double TWO_COLUMN_LEGEND_DELY = 0.042;

const double ONE_COLUMN_LEGEND_X1 = 0.71;
const double ONE_COLUMN_LEGEND_X2= 0.9;
const double ONE_COLUMN_LEGEND_Y2 = 0.9;
const double ONE_COLUMN_LEGEND_DELY = 0.030;

const double ONE_COLUMN_LEGEND_X1_SPLIT = 0.74;
const double ONE_COLUMN_LEGEND_X2_SPLIT= 0.94;
const double ONE_COLUMN_LEGEND_Y2_SPLIT = 0.90;
const double ONE_COLUMN_LEGEND_DELY_SPLIT = 0.045;

// + ATLAS label settings
const double RATIO_LABEL_X = 0.18;
const double RATIO_LABEL_Y = 0.86;
const double SINGLE_LABEL_X = 0.2;
const double SINGLE_LABEL_Y = 0.87;

// + Text settings 
const double RATIO_TEXT_SIZE = 0.03;
const double SINGLE_TEXT_SIZE = 0.021;
const double RATIO_TEXT_BASE_Y = 0.10;
const double RATIO_TEXT_INC_Y = RATIO_TEXT_BASE_Y / 2.;
const double SINGLE_TEXT_BASE_Y = 0.07;
const double SINGLE_TEXT_INC_Y = SINGLE_TEXT_BASE_Y / 2.;

// + Main plot y-axis settings
const double SINGLE_MAIN_Y_OFFSET = 1.9;
const double RATIO_MAIN_Y_OFFSET = 1.6;
const double Y_AXIS_MULTIPLIER = 1.4;
const double LOGY_AXIS_MULTIPLIER = 100;

// + Ratio plot y-axis settings
const double RATIO_Y_OFFSET = 0.6;
const double RATIO_Y_OFFSET_FROM_1 = 0.5;
const double RATIO_TICKS = 505;
