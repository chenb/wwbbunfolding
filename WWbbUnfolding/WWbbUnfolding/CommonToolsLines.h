#pragma once

#include "TLine.h"
#include "TArrow.h"

#include "json.hpp"

#include "Environment.h"

namespace COMMONTOOLSLINES{
   void DrawSplitCanvasSeparator(double, double);
   void DrawRatioPlotDashedLines(double, double, double);
   //template<class TH> void DrawRatioPlotLines(TH*, const PLOTPARAMS&, double fYOffset = 0.5);
   //template<class TG> void DrawOutOfRangeArrows(TG*, double fYOffset = 0.5);
};

//template<class TH>
//void CommonToolsLines::DrawRatioPlotLines(TH* fInputHist, const PLOTPARAMS& fPlotParams, double fYOffset){ 
   //// + Draw the horizontal hashed lines in the ratio plot 
   //double LinexMin = (fPlotParams.XMIN == -999) ? fInputHist->GetXaxis()->GetXmin() : fPlotParams.XMIN;
   //double LinexMax = (fPlotParams.XMAX == -999) ? fInputHist->GetXaxis()->GetXmax() : fPlotParams.XMAX;

//}

//template<class TG>
//void CommonToolsLines::DrawOutOfRangeArrows(TG* fInputGraph, double fYOffset){
   //// + Loop through the ratio graph data points and check if any point falls out of the visible range and replace these with arrows
   //for(int i = 0; i < fInputGraph->GetN(); i++){
      //double ix = fInputGraph->GetPointX(i);
      //double iy = fInputGraph->GetPointY(i);

      //if(iy > (1. - fYOffset) && iy < (1. + fYOffset)) continue;
      
      //double iyLow = (iy < (1. - fYOffset)) ? (1. - fYOffset) + fYOffset/5. : (1. + fYOffset) - fYOffset/5.;
      //double iyHigh = (iy < (1. - fYOffset)) ? (1. - fYOffset) + fYOffset/29.5 : (1. + fYOffset) - fYOffset/29.5;
      //TArrow* iArrow = new TArrow(ix, iyLow, ix, iyHigh, 0.01, "|>");
      //iArrow->SetLineWidth(2);
      //iArrow->SetLineColor(kRed);
      //iArrow->SetFillColor(kRed);
      //iArrow->Draw();
   //}
//}

