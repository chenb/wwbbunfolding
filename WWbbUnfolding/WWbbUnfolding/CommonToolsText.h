#pragma once

#include <sstream>

#include "json.hpp"

#include "Environment.h"
#include "Constants.h"

#include "TLatex.h"

namespace COMMONTOOLSTEXT{
   void SplitAndPrintText(const std::string&, bool);
   void DrawATLASLabel(double, double, bool, const std::string&);
};
