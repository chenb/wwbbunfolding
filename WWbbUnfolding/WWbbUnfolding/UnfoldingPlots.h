#pragma once

#include "TH1.h"
#include "TH2.h"

#include "json.hpp"

namespace UnfoldingPlots{
   void GeneratePlots();
   void PlotProjections(nlohmann::json, nlohmann::json, nlohmann::json);
   void PerformUnfolding(nlohmann::json, nlohmann::json, nlohmann::json, nlohmann::json, bool fSubtractionMigrationBins = true);

   TH1D* GetDataHistogram(nlohmann::json);
   TH1D* GetBGHistogram(nlohmann::json);
   TH1D* GetTruthHistogram(nlohmann::json);
   TH2D* GetMigrationMatrix(nlohmann::json);
};
