#pragma once

#include <vector>
#include <string>

#include "TFile.h"
#include "TColor.h"

namespace ENVIRONMENT{
   void InitializeInputFile(const std::string&);
   void InitializeOutputFile(const std::string&);
   void InitializeJsonFile(const std::string&);
   void ActivateTNSamples();

   namespace FILES{
      inline bool ACTIVATE_TN_SAMPLES = false;
      inline std::string INPUT_FILE_NAME = "";
      inline std::string OUTPUT_FILE_NAME = "";
      inline std::string INPUT_JSON_FILE_NAME = "";
      inline TFile* INPUT_FILE = nullptr;
   };

   namespace BASE_COLORS{
      const int WHITE = 0;
      const int BLACK = 1;
      const int GRAY = 921;
      const int BLUE = 597;
      const int RED = 629;
      const int GREEN = 418;
      const int MAGENTA = 613;
      const int CYAN = 433;
      const int YELLOW = 397;
      const int ORANGE = 797;
   };

   namespace FILL_COLORS{ 
      inline int PURPLE = TColor::GetFreeColorIndex();
      inline TColor* PURPLE_C = new TColor(PURPLE, 94. / 255., 79. / 255., 162. / 255.);

      inline int BLUE = TColor::GetFreeColorIndex();
      inline TColor* BLUE_C = new TColor(BLUE, 50. / 255., 136. / 255., 189. / 255.);

      inline int LIGHT_GREEN = TColor::GetFreeColorIndex();
      inline TColor* LIGHT_GREEN_C = new TColor(LIGHT_GREEN, 171. / 255., 221. / 255., 164. / 255.);
      
      inline int GREEN = TColor::GetFreeColorIndex();
      inline TColor* GREEN_C = new TColor(GREEN, 102. / 255., 194. /255., 165. / 255.);

      inline int BRIGHT_GREEN = TColor::GetFreeColorIndex();
      inline TColor* BRIGHT_GREEN_C = new TColor(BRIGHT_GREEN, 230. / 255., 245. / 255., 152. / 255.);

      inline int LIGHT_ORANGE = TColor::GetFreeColorIndex();
      inline TColor* LIGHT_ORANGE_C = new TColor(LIGHT_ORANGE, 254. / 255., 224. / 255., 139. / 255.);

      inline int ORANGE = TColor::GetFreeColorIndex();
      inline TColor* ORANGE_C = new TColor(ORANGE, 253. / 255., 174. / 255., 97. / 255.);

      inline int BRIGHT_ORANGE = TColor::GetFreeColorIndex();
      inline TColor* BRIGHT_ORANGE_C = new TColor(BRIGHT_ORANGE, 244. / 255., 109. / 255., 67. / 255.);

      inline int RED = TColor::GetFreeColorIndex();
      inline TColor* RED_C = new TColor(RED, 213. / 255., 62. / 255., 79. / 255.);

      inline int BRIGHT_RED = TColor::GetFreeColorIndex();
      inline TColor* BRIGHT_RED_C = new TColor(BRIGHT_RED, 158. / 255., 1. / 255., 66. / 255.);
   };

   const std::vector<int> LINE_COLORS = {BASE_COLORS::BLUE, BASE_COLORS::RED, BASE_COLORS::GREEN, BASE_COLORS::MAGENTA, BASE_COLORS::CYAN, BASE_COLORS::YELLOW, BASE_COLORS::ORANGE};
   const std::vector<std::string> MC_SOURCES = {"TTBAR_INCLUSIVE", "SINGLETOP_INCLUSIVE_WT_DR", "SINGLETOP_INCLUSIVE_NOWT", "WJETS", "ZJETS", "RARETOP", "MULTIBOSON", "DIBOSON"};
   const std::vector<std::string> MC_SOURCES_SIGNAL = {"SINGLETOP_INCLUSIVE_NOWT", "RARETOP", "SINGLETOP_INCLUSIVE_WT_DR", "TTBAR_INCLUSIVE"};
   const std::vector<std::string> MC_SOURCES_BG = {"DIBOSON", "MULTIBOSON", "ZJETS", "WJETS"};
};
