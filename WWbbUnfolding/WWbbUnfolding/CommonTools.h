#pragma once

#include "json.hpp"

#include "TH1.h"
#include "TH2.h"
#include "THStack.h"

#include <memory>

struct CONFIG{
   std::string REGION = "";
   std::string LEGENDENTRY = "";
   std::string HISTNAME = "";
   std::string FILENAME = "";
   std::string XLABEL = "";
   std::string YLABEL = "";
   std::string TEXT = "";
   bool LOGY = false;
   bool NORMALIZE = false;
   bool SAVEPLOT = false; 
};

struct VISUAL{
   int LINE_COLOR = kBlack;
   int LINE_STYLE = 1;
   int LINE_WIDTH = 1;

   int FILL_COLOR = kWhite;
   int FILL_STYLE = 1001;

   int MARKER_COLOR = kBlack;
   int MARKER_STYLE = kFullCircle;
   double MARKER_SIZE = 0.9;
};

struct RANGE{
   double XMIN = -1.;
   double XMAX = -1.;
   double YMIN = -1.;
   double YMAX = -1.;
};

namespace COMMONTOOLS{
   CONFIG ConvertJsonToStruct(const nlohmann::json&);
   TH1D* Initialize1DHistogram(const std::string&, const std::string&, const std::string&, const VISUAL& fHistVisuals = {});
   TH2D* Initialize2DHistogram(const std::string&, const std::string&, const std::string&);
   void SetHistogramVisuals(TH1D*, const VISUAL& fHistVisuals = {});

   RANGE GetHistRange(TH1D*, THStack*);
   RANGE GetHistRange(TH1D*, TH1D*);
   RANGE GetHistRange(TH1D*);
   RANGE GetHistRange(TH2D*);
   RANGE GetHistRange(const std::vector<TH1D*>&);


   //double ConvertToUserX(double);
   //double ConvertToUserY(double);
};


