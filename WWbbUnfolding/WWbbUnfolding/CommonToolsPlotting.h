#pragma once

#include <string>

#include <string>
#include <sstream>
#include <vector>

#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "THStack.h"
#include "TPad.h"
#include "TGraphAsymmErrors.h"

#include "Environment.h"
#include "Constants.h"
#include "CommonTools.h"

#include "json.hpp"

namespace COMMONTOOLSPLOTTING{
   void PlotSplitCanvasMainPad(TCanvas*, const CONFIG&, TH1D*, THStack*);
   void PlotSplitCanvasMainPad(TCanvas*, const CONFIG&, TH1D*, TH1D*);
   void PlotSplitCanvasRatioPad(TCanvas*, const CONFIG&, TH1D*, THStack*, const std::string&, double  fYOffset = 0.5);
   void PlotSplitCanvasRatioPad(TCanvas*, const CONFIG&, TH1D*, TH1D*, const std::string&, double  fYOffset = 0.5);
   void PlotSingleCanvas1D(TCanvas*, const CONFIG&, TH1D*);
   void PlotSingleCanvas2D(TCanvas*, const CONFIG&, TH2D*, double fzMin = -999., double fzMax = -999.);
   void PlotSingleCanvas1DOverlay(TCanvas*, const CONFIG&, const std::vector<TH1D*>&);

   void SetupSplitCanvasMainPad(const std::string&, const CONFIG&, TH1D*, THStack*);
   void SetupSplitCanvasMainPad(const std::string&, const CONFIG&, TH1D*, TH1D*);
   void SetupSplitCanvasRatioPad(const std::string&, const CONFIG&, TH1D*, THStack*,  const std::string&, double fYOffset = 0.5);
   void SetupSplitCanvasRatioPad(const std::string&, const CONFIG&, TH1D*, TH1D*,  const std::string&, double fYOffset = 0.5);
   void SetupSingleCanvas1D(TCanvas*, const CONFIG&, TH1D*);
   void SetupSingleCanvas2D(TCanvas*, const CONFIG&, TH2D*, double fzMin = -999., double fzMax = -999.);
   void SetupSingleCanvas1DOverlay(TCanvas*, const CONFIG&, const std::vector<TH1D*>&);

   void SetupSplitCanvasMainPadMargins(TPad*); 
   void SetupSplitCanvasRatioPadMargins(TPad*);
   void SetupSingleCanvas1DMargins(TCanvas*);
   void SetupSingleCanvas2DMargins(TCanvas*);
   void SetupSingleCanvas1DOverlayMargins(TCanvas*);

   void SetupSplitCanvasMainPadFrame(TH1F*, const CONFIG&);
   void SetupSplitCanvasRatioPadFrame(TH1F*, const CONFIG&, const std::string&);
   void SetupSingleCanvas1DFrame(TH1F*, const CONFIG&);
   void SetupSingleCanvas2DFrame(TH1F*, const CONFIG&, double fzMin = -999., double fzMax = -999.);
   void SetupSingleCanvas1DOverlayFrame(TH1F*, const CONFIG&);
};

////template<class TH>
////void CommonToolsPlotting::PlotSingleCanvas1DOverlay(TCanvas* fCanvas, const PLOTPARAMS& fPlotParams, std::vector<TH*> fInputHistsVec, double fyMin, double fyMax){
   ////fCanvas->SetTopMargin(CANVAS_1D_TOP_MARGIN);
   ////fCanvas->SetBottomMargin(CANVAS_1D_BOTTOM_MARGIN);
   ////fCanvas->SetLeftMargin(CANVAS_1D_LEFT_MARGIN);
   ////fCanvas->SetRightMargin(CANVAS_1D_RIGHT_MARGIN);
   ////fCanvas->Draw();
   ////fCanvas->cd();
  
   ////std::vector<TH*> ClonedHistVec = {};
   ////for(unsigned int i = 0; i < fInputHistsVec.size(); i++){
      ////TH* iInputHist = (TH*)fInputHistsVec[i]->Clone();
      //////iInputHist->SetLineColor(fInputHistsVec[i]->GetLineColor());
      //////iInputHist->SetMarkerColor(fInputHistsVec[i]->GetMarkerColor());
      //////iInputHist->SetMarkerSize(MARKER_SIZE);
      //////iInputHist->GetXaxis()->SetTitle(boost::get<std::string>(fParamsMap["xLabel"]).c_str());
      //////iInputHist->GetYaxis()->SetTitle(boost::get<std::string>(fParamsMap["yLabel"]).c_str());
      //////iInputHist->GetYaxis()->SetTitleOffset(SINGLE_MAIN_Y_OFFSET);
      ////iInputHist->Draw("HIST E1 SAME");
      ////ClonedHistVec.push_back(iInputHist);
   ////}

   ////CommonTools::SetHistLimits(ClonedHistVec, fPlotParams);
////}

//template<class TH>
//void CommonToolsPlotting::SetupSingleCanvasOverlay(TCanvas* fMainCanvas, const PLOTPARAMS& fPlotParams, std::vector<TH*> fInputHists){
   //fMainCanvas->SetTopMargin(CANVAS_1D_TOP_MARGIN);
   //fMainCanvas->SetBottomMargin(CANVAS_1D_BOTTOM_MARGIN);
   //fMainCanvas->SetLeftMargin(CANVAS_1D_LEFT_MARGIN);
   //fMainCanvas->SetRightMargin(CANVAS_1D_RIGHT_MARGIN);
   //fMainCanvas->Draw();
   //fMainCanvas->cd();

   //XYRANGE InputXYRange = CommonTools::GetXYRange(fInputHists);

   //TH1F* MainCanvasFrame = fMainCanvas->DrawFrame(InputXYRange.XMIN, InputXYRange.YMIN, InputXYRange.XMAX, InputXYRange.YMAX * Y_AXIS_MULTIPLIER);
   //MainCanvasFrame->GetXaxis()->SetTitle(fPlotParams.XLABEL.c_str());
   //MainCanvasFrame->GetYaxis()->SetTitle(fPlotParams.YLABEL.c_str());
   //MainCanvasFrame->GetYaxis()->SetTitleOffset(RATIO_MAIN_Y_OFFSET);
   //MainCanvasFrame->Draw();
//}
