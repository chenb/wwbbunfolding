#!/usr/bin/python3

import os

# Input user settings to turn on/off certain plots
input_file_name = 'FakesApply.SR2_Combined.root'
output_file_name = 'FakesApply.SR2.pdf'

DoStackedPlots = True
DoSinglePlots = False
DoFakeRatePlots = False
DoEfficiencyPlots = False
DoUnfoldingPlots = False
DoOverlayPlots = False

executable_dir = '/home/charlie/Desktop/WWbbPostProcessing/build'
executable_name = 'WWbbPostProcessing'
executable_path = os.path.join(executable_dir, executable_name)

# Begin main function to read user settings and pass these into the executable as command line arguments
# Command line arguments are read in using the boost::program_options library
if __name__ == "__main__":
   executable_path = f'{executable_path} --input {input_file_name} --output {output_file_name}'
   if(DoStackedPlots): executable_path += ' --stack'
   if(DoSinglePlots): executable_path += ' --single'
   if(DoFakeRatePlots): executable_path += ' --fake'
   if(DoEfficiencyPlots): executable_path += ' --efficiency'
   if(DoUnfoldingPlots): executable_path += ' --unfold'
   if(DoOverlayPlots): executable_path += ' --overlay'

   os.system(executable_path)
